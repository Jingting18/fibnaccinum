#pragma once
class Test
{
public:
	Test(int num);
	~Test();
	bool isPrimeNum(long long num);
	void printNum(long long* num, int length);
	long long* generateNum(int num);

private:
	long long* data;
};

