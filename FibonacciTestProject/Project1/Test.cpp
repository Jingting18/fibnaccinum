#include "Test.h"
#include <iostream>
using namespace std;

Test::Test(int num)
{
	data = new long long[num];;
}


Test::~Test()
{
	delete[] data;
}

bool Test::isPrimeNum(long long num){
	bool isPrime = true;
	int upperBound = sqrt(num);
	for (int i = 2; i <= upperBound; ++i)
	{
		if (num % i == 0)
		{
			isPrime = false;
			break;
		}
	}
	return isPrime;
}

long long* Test::generateNum(int num){
	data[0] = 1;
	data[1] = 1;
	for (int i = 2; i < num; i++){
		data[i] = data[i - 2] + data[i - 1];
	}
	for (int i = 0; i < num; i++){
		cout << data[i] << " " << endl;
	}
	return data;
}

void Test::printNum(long long* data, int length){
	int idx = 0;
	while (idx < length){
		cout << data[idx]<<": ";
		if (data[idx] % 15 == 0)
		{
			cout <<" "<< "FizzBuzz"<<endl;
			idx++;
			continue;
		}
		else if (data[idx] % 5 == 0){
			cout << " " << "Fizz";
		}
		else if (data[idx] % 3 == 0){
			cout << " " << "Buzz";
		}
		if (isPrimeNum(data[idx])){
			cout << " " << "BuzzFizz";
		}
		else{
			cout << " " << data[idx];
		}
		cout << endl;
		idx++;
	}
}