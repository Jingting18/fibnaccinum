/*
Author:  Jingting Zeng
Date : 3 / 19 / 2017
*/

#include "Test.h""
void main()
{
	int fibLength = 50;
	Test test(fibLength);

	long long* data = test.generateNum(fibLength);
	test.printNum(data, fibLength);
}